/// @author Romain DUCHER<ducher.romain@gmail.com>
///
/// @section LICENSE
///
/// Copyright Romain DUCHER 2023
///
/// This software is a computer program whose purpose is to input working hours
/// and export them in a spreadsheet file.
///
/// This software is governed by the CeCILL-C license under French law and
/// abiding by the rules of distribution of free software. You can use,
/// modify and/ or redistribute the software under the terms of the CeCILL-C
/// license as circulated by CEA, CNRS and INRIA at the following URL
/// "http://www.cecill.info".
///
/// As a counterpart to the access to the source code and rights to copy,
/// modify and redistribute granted by the license, users are provided only
/// with a limited warranty and the software's author, the holder of the
/// economic rights, and the successive licensors have only limited liability.
///
/// In this respect, the user's attention is drawn to the risks associated
/// with loading, using, modifying and/or developing or reproducing the
/// software by the user in light of its specific status of free software,
/// that may mean that it is complicated to manipulate, and that also
/// therefore means that it is reserved for developers and experienced
/// professionals having in-depth computer knowledge. Users are therefore
/// encouraged to load and test the software's suitability as regards their
/// requirements in conditions enabling the security of their systems and/or
/// data to be ensured and, more generally, to use and operate it in the
/// same conditions as regards security.
///
/// The fact that you are presently reading this means that you have had
/// knowledge of the CeCILL-C license and that you accept its terms.
import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router';

import routes from 'src/router/routes';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  return Router;
});
