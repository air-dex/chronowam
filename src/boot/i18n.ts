/// @author Romain DUCHER<ducher.romain@gmail.com>
///
/// @section LICENSE
///
/// Copyright Romain DUCHER 2023
///
/// This software is a computer program whose purpose is to input working hours
/// and export them in a spreadsheet file.
///
/// This software is governed by the CeCILL-C license under French law and
/// abiding by the rules of distribution of free software. You can use,
/// modify and/ or redistribute the software under the terms of the CeCILL-C
/// license as circulated by CEA, CNRS and INRIA at the following URL
/// "http://www.cecill.info".
///
/// As a counterpart to the access to the source code and rights to copy,
/// modify and redistribute granted by the license, users are provided only
/// with a limited warranty and the software's author, the holder of the
/// economic rights, and the successive licensors have only limited liability.
///
/// In this respect, the user's attention is drawn to the risks associated
/// with loading, using, modifying and/or developing or reproducing the
/// software by the user in light of its specific status of free software,
/// that may mean that it is complicated to manipulate, and that also
/// therefore means that it is reserved for developers and experienced
/// professionals having in-depth computer knowledge. Users are therefore
/// encouraged to load and test the software's suitability as regards their
/// requirements in conditions enabling the security of their systems and/or
/// data to be ensured and, more generally, to use and operate it in the
/// same conditions as regards security.
///
/// The fact that you are presently reading this means that you have had
/// knowledge of the CeCILL-C license and that you accept its terms.
import { boot } from 'quasar/wrappers';
import { createI18n } from 'vue-i18n';

import messages from 'src/i18n';

export type MessageLanguages = keyof typeof messages;
// Type-define 'en-US' as the master schema for the resource
export type MessageSchema = (typeof messages)['en-US'];

// See https://vue-i18n.intlify.dev/guide/advanced/typescript.html#global-resource-schema-type-definition
/* eslint-disable @typescript-eslint/no-empty-interface */
declare module 'vue-i18n' {
  // define the locale messages schema
  export interface DefineLocaleMessage extends MessageSchema {}

  // define the datetime format schema
  export interface DefineDateTimeFormat {}

  // define the number format schema
  export interface DefineNumberFormat {}
}
/* eslint-enable @typescript-eslint/no-empty-interface */

// const supportedLocales = ['en-US', 'fr-FR']
const fallbackLocale = 'en-US';

//const getNavLang  = () => supportedLocales.includes(navigator.language) ? navigator.language : fallbackLocale

export default boot(({ app }) => {
  const i18n = createI18n({
    locale: navigator.language,
    fallbackLocale,
    legacy: false,
    messages,
  });

  // TODO: Dynamical load of language packs

  // Set i18n instance on app
  app.use(i18n);
});
