# Chrono WAM (chronowam)

Chrono WAM - Working hours input

## Summary

TODO

## Dev

Each time, 3 ways to do it are presented:

1. Using `package.json` scripts: `npm run etc.`.
2. The "real" CLI instruction, using CLI executable.
3. Using `npx` if the executable is not in your PATH.

Prerequisite to everything: **Installing dependendencies**:

```shell
npm install
```

### Running in dev

#### Without Vue DevTools

```shell
npm run dev
```

```shell
quasar dev -m electron
```

```shell
npx quasar dev -m electron
```

#### With Vue DevTools

```shell
npm run dev -- --devtools
```

```shell
quasar dev -m electron --devtools
```

```shell
npx quasar dev -m electron --devtools
```

### Building from source

```shell
npm run build
```

```shell
quasar build -m electron
```

```shell
npx quasar dev -m electron --devtools
```

You can also add a debug flag with `-d` (or `--debug`):

```shell
npm run build -- -d
```

```shell
quasar build -m electron -d
```

```shell
npx quasar build -m electron -d
```

### Running in prod

Project must be built from sources before.

```shell
npm run start
```

```shell
electron-forge start
```

```shell
npx electron-forge start
```

### Creating an installer

Installers are created thanks to Electron Forge. You must build project (see corresponding section) before packaging.

#### Packaging

It corresponds to the "packaging" part of Electron Forge.

```shell
npm run package
```

```shell
electron-forge package
```

```shell
npx electron-forge package
```

#### Creating the installer

It corresponds to the "makers" part of Electron Forge. "Packaging" part must be performed before

```shell
npm run create-installer
```

```shell
electron-forge make --skip-package
```

```shell
npx electron-forge make --skip-package
```

#### Both packaging and creating installer

It corresponds to the "makers" part of Electron Forge. "Packaging" part must be performed before.

```shell
npm run make
```

```shell
electron-forge make
```

```shell
npx electron-forge make
```

### Full build process, from sources to installers

```shell
npm install
npm run build
npm run make
```

```shell
quasar build -m electron
electron-forge make
```

```shell
npx quasar build -m electron
npx electron-forge make
```

## Code architecture

### Files & folders

Some files and and folders are not versioned

```
/---+                         ##############################################
    |                         #              Quasar Framework              #
    |                         ##############################################
    +---/index.html           # SPA HTML page, also SPA entrypoint         #
    |                         #                                            #
    +---/public/              # Static assets                              #
    |                         #                                            #
    +---/src/                 # SPA sources                                #
    |                         #                                            #
    +---/src-electron/        # Electron-related sources                   #
    |                         #                                            #
    +---/quasar.config.js     # Quasar config                              #
    |                         #                                            #
    +---/.quasar/             # quasar dev stuff                           #
    |                         #                                            #
    +---/dist/                # Where Quasar puts what it builds           #
    |                         #                                            #
    +---/out/                 # Where Electron Forge puts what it packages #
    |                         ##############################################
    |
    |                         ############################
    |                         #       Node.js & NPM      #
    |                         ############################
    +---/package.json         # Node.js Project file     #
    |                         #                          #
    +---/package-lock.json    # Packages dependencies    #
    |                         #                          #
    +---/node_modules/        # Node dependencies folder #
    |                         #                          #
    +---/.npmrc               # NPM config file          #
    |                         ############################
    |
    |                         ############################
    |                         # Dev tools configurations #
    |                         ############################
    +---/postcss.config.js    # PostCSS                  #
    |                         #                          #
    +---/tsconfig.json        # TypeScript               #
    |                         #                          #
    +---/forge.config.json    # Electron Forge           #
    |                         ############################
    |
    +---/LICENCE              # Project license (CeCiLL-C)
    |
    +---/README.md            # This README
    |
    |                         ########################
    |                         #          git         #
    |                         ########################
    +---/.git/                # Git folder           #
    |                         #                      #
    +---/.gitignore           # Files ignored by git #
    |                         ########################
    |
    |                         ###############################
    |                         # Code quality configurations #
    |                         ###############################
    +---/.editorconfig        # Editorconfig                #
    |                         #                             #
    +---/.prettierrc          # Prettier                    #
    |                         #                             #
    +---/.eslintrc.js         # ESLint                      #
    |                         #                             #
    +---/.eslintignore        # Files ignored by ESLint     #
    |                         ###############################
    |
    |                         ####################
    |                         # IDE integrations #
    |                         ####################
    +---/.vscode/             # VS Code          #
    |                         #                  #
    +---/.idea/               # IntelliJ IDEA    #
                              ####################
```

### Application architecture

Chrono WAM is an Electron application developed using Quasar Framework. It makes full use of the 2 Electron processes:

* The renderer process contains the Vue.js Single Page Application. It acts as application frontend.
* The main process contains the OS-related backend operations: file access, SQL operation. It acts as application
  backend.
* The 2 processes communicate using Electron's IPC: https://www.electronjs.org/docs/latest/tutorial/ipc

#### Frontend

Frontend sources are located in the `/src/` folder.

**Remarkable files and folders:**

```
/src/
|
|                                 #####################
+---/router/                      #      Routing      #
|   |                             #####################
|   +---/index.ts                 # Router definition #
|   |                             #                   #
|   +---/routes.ts                # Routes definition #
|                                 #####################
|
|                                 #####################################################
|                                 #           Vue.js Single File Components           #
|                                 #####################################################
+---/App.vue                      # Quasar Application                                #
|                                 #                                                   #
+---/components/                  # Reusable components                               #
|   |                             #                                                   #
|   +---/layout/                  # Layout components, just like header and drawers   #
|                                 #                                                   #
+---/layouts/                     # Quasar Layouts                                    #
|   |                             #                                                   #
|   +---/CWamLayout.vue           # Layout used by all pages                          #
|                                 #                                                   #
+---/pages/                       # SFCs rendered in the "Page" part of Quasar Layout #
|                                 #####################################################
|
|                                 ####################################
+---/stores/                      #           Pinia stores           #
|   |                             ####################################
|   +---/index.ts                 # Pinia instance                   #
|   |                             #                                  #
|   +---/activities.ts            # Store dealing with activities    #
|   |                             #                                  #
|   +---/config.ts                # Chrono WAM's configuration store #
|                                 ####################################
|
|                                 #############################################################
+---/api/                         #                 Communication with backend                #
|   |                             #############################################################
|   +---/backendAPI.ts            # Defines the type representing application's backend       #
|   |                             #                                                           #
|   +---/api.ts                   # Making backend object independent from its implementation #
|                                 #############################################################
|
|                                 #####################
+---/boot/                        # Quasar boot files #
|   |                             #####################
|   +---/i18n.js                  # i18n boot         #
|                                 #####################
|
|                                 #######################################
+---/css/                         #                Style                #
|   |                             #######################################
|   +---/app.scss                 # Root file for own style definitions #
|   |                             #                                     #
|   +---/quasar.variables.scss    # Overriding Quasar variables         #
|                                 #######################################
|
+---/assets/                      # Dynamic assets
|
|                                 ########################
+---/i18n/                        # Internationalization #
    |                             ########################
    +---/en-US.ts                 # English translations #
    |                             #                      #
    +---/fr-FR.ts                 # French translations  #
    |                             #                      #
    +---/index.ts                 # Messages module      #
                                  ########################
```

**Frontend architecture**

```mermaid
flowchart TD;
  A[Vue.js SFCs]-->B
  B[Pinia stores]-->A;
  B-->C;
  C[Frontend's API object]-->B;
  D[Frontend's window.API]<-->C;
```

#### Backend

Backend sources are located in the `/src-electron/` folder because it is contained in Electron's main process and that's
where Electron's main process code should be put in a Quasar application.

**Remarkable files and folders:**

```
/src-electron/
|
|                            #######################################################
+---/ipc/                    #             Inter-Process Communication             #
|   |                        #######################################################
|   +---/handle.ts           # Listeners to IPC events, reprensented by "channels" #
|   |                        #                                                     #
|   +---/channelNames.ts     # IPC channel names                                   #
|   |                        #                                                     #
|   +---/api.ts              # Mapping channel names and functions to execute      #
|   |                        #                                                     #
|   +---/expose.ts           # Exposing backend API.                               #
|                            #######################################################
|
|                            ##########################
+---/services/               # Backend business logic #
|   |                        ##########################
|   +---/config.ts           # Configuration          #
|   |                        #                        #
|   +---/activities.ts       # Activities             #
|                            ##########################
|
|                            ###################################################
+---/data-access/            #                   Data access                   #
|   |                        ###################################################
|   +---/mappers/            # Mappers (database to business types)            #
|   |   |                    #                                                 #
|   |   +---/activity.ts     # - Mappers for activities                        #
|   |   |                    #                                                 #
|   |   +---/topic.ts        # - Mappers for activity topics                   #
|   |                        #                                                 #
|   +---/types/              # Types endemic to data access                    #
|   |   |                    #                                                 #
|   |   +---/cvh.d.ts        # - Type of Nconf handler                         #
|   |   |                    #                                                 #
|   |   +---/dbTypes.d.ts    # - Database rows                                 #
|   |                        #                                                 #
|   +---/handlers/           # Handlers handling infrastructures               #
|       |                    #                                                 #
|       +---/db.ts           # - SQLite database into activities savefile      #
|       |                    #                                                 #
|       +---/files.ts        # - Files utilities (through Node.js classes)     #
|       |                    #                                                 #
|       +---/nconf.ts        # - Configuration values (through nconf)          #
|       |                    #                                                 #
|       +---/paths.ts        # - File path utilities (through Node.js classes) #
|                            ###################################################
|
|                            #############################
|                            #  Electron's "main" files  #
|                            #############################
+---/electron-main.ts        # Application entrypoint    #
|                            #                           #
+---/electron-preload.ts     # Electron's preload script #
|                            #############################
|
+---/icons/                  # Application icon
```

**Backend architecture:**

```mermaid
flowchart TD;
  A[Frontend's window.API]<-->B;
  B[Backend's API object]-->C;
  C[Services]-->B;
  C-->D;
  D[Data access handlers]-->C;
  E[nconf]-->D;
  D-->E;
  F[SheetJS]-->D;
  D-->F;
  G[SQLite]-->D;
  D-->G;
  H[Node.js utils for files]-->D;
  D-->H;
  I[Node.js utils for paths]-->D;
  D-->I;
```

Backend listeners are invoked through IPC events. All listeners are registered in the `API` object defined
in `/src-electron/ipc/api.ts`.

At launch, `API` is exposed in the `window.API` JS object thanks to `/src-electron/electron-preload.ts` preload script,
which bridges Electron's process. For this, it executes the exposing function defined in `/src-electron/ipc/expose.ts`.

Listeners defined in `API` uses "services" to do what they must do. Those services are in `/src-electron/services/`.

The services rely on data-access entities called "handlers". Those handlers are in `/src-electron/data-access/handlers/`.

### Full architure

```mermaid
flowchart TD;
  A[Vue.js SFCs]-->B;
  B[Pinia stores]-->A;
  B-->C;
  C[Frontend's API object]-->B;
  D[Frontend's window.API]<-->C;
  D<-->E;
  E[Backend's API object]-->F;
  F[Backend services]-->E;
  F-->G;
  G[Data access handlers]-->F;
  H[nconf]-->G;
  G-->H;
  I[SheetJS]-->G;
  G-->I;
  J[SQLite]-->G;
  G-->J;
  K[Node.js utils for files]-->G;
  G-->K;
  L[Node.js utils for paths]-->G;
  G-->L;
```

## License

Chrono WAM is under CeCILL-C licence (LGPL's French equivalent).
