/// @author Romain DUCHER<ducher.romain@gmail.com>
///
/// @section LICENSE
///
/// Copyright Romain DUCHER 2023
///
/// This software is a computer program whose purpose is to input working hours
/// and export them in a spreadsheet file.
///
/// This software is governed by the CeCILL-C license under French law and
/// abiding by the rules of distribution of free software. You can use,
/// modify and/ or redistribute the software under the terms of the CeCILL-C
/// license as circulated by CEA, CNRS and INRIA at the following URL
/// "http://www.cecill.info".
///
/// As a counterpart to the access to the source code and rights to copy,
/// modify and redistribute granted by the license, users are provided only
/// with a limited warranty and the software's author, the holder of the
/// economic rights, and the successive licensors have only limited liability.
///
/// In this respect, the user's attention is drawn to the risks associated
/// with loading, using, modifying and/or developing or reproducing the
/// software by the user in light of its specific status of free software,
/// that may mean that it is complicated to manipulate, and that also
/// therefore means that it is reserved for developers and experienced
/// professionals having in-depth computer knowledge. Users are therefore
/// encouraged to load and test the software's suitability as regards their
/// requirements in conditions enabling the security of their systems and/or
/// data to be ensured and, more generally, to use and operate it in the
/// same conditions as regards security.
///
/// The fact that you are presently reading this means that you have had
/// knowledge of the CeCILL-C license and that you accept its terms.
import { app, BrowserWindow, nativeTheme } from 'electron';
import path from 'path';
import os from 'os';
import handleAPI from 'ipc/handle';

// needed in case process is undefined under Linux
const platform = process.platform || os.platform();

try {
  if (platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(
      path.join(app.getPath('userData'), 'DevTools Extensions'),
    );
  }
} catch (_) {
  // eslint
}

let mainWindow: BrowserWindow | undefined;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    icon: path.resolve(__dirname, 'icons/icon.png'), // tray icon
    width: 1000,
    height: 600,
    useContentSize: true,
    webPreferences: {
      contextIsolation: true,
      // More info: https://v2.quasar.dev/quasar-cli-vite/developing-electron-apps/electron-preload-script
      preload: path.resolve(
        __dirname,
        process.env.QUASAR_ELECTRON_PRELOAD as string,
      ),
    },
    autoHideMenuBar: true,
  });

  mainWindow.loadURL(process.env.APP_URL as string);

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools();
  } else {
    // we're on production; no access to devtools pls
    mainWindow.webContents.on('devtools-opened', () => {
      mainWindow?.webContents.closeDevTools();
    });
  }

  mainWindow.on('closed', () => {
    mainWindow = undefined;
  });
}

app.whenReady().then(() => {
  handleAPI();
  createWindow();
});

app.on('window-all-closed', () => {
  if (platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === undefined) {
    createWindow();
  }
});
