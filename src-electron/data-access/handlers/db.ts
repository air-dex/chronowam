/// @author Romain DUCHER<ducher.romain@gmail.com>
///
/// @section LICENSE
///
/// Copyright Romain DUCHER 2023
///
/// This software is a computer program whose purpose is to input working hours
/// and export them in a spreadsheet file.
///
/// This software is governed by the CeCILL-C license under French law and
/// abiding by the rules of distribution of free software. You can use,
/// modify and/ or redistribute the software under the terms of the CeCILL-C
/// license as circulated by CEA, CNRS and INRIA at the following URL
/// "http://www.cecill.info".
///
/// As a counterpart to the access to the source code and rights to copy,
/// modify and redistribute granted by the license, users are provided only
/// with a limited warranty and the software's author, the holder of the
/// economic rights, and the successive licensors have only limited liability.
///
/// In this respect, the user's attention is drawn to the risks associated
/// with loading, using, modifying and/or developing or reproducing the
/// software by the user in light of its specific status of free software,
/// that may mean that it is complicated to manipulate, and that also
/// therefore means that it is reserved for developers and experienced
/// professionals having in-depth computer knowledge. Users are therefore
/// encouraged to load and test the software's suitability as regards their
/// requirements in conditions enabling the security of their systems and/or
/// data to be ensured and, more generally, to use and operate it in the
/// same conditions as regards security.
///
/// The fact that you are presently reading this means that you have had
/// knowledge of the CeCILL-C license and that you accept its terms.
import sqlite3 from 'sqlite3';
import { open, Database } from 'sqlite';
import { date } from 'quasar';
import { Activity, Topic } from 'logic/types';
import { activitiesDbFilePath } from 'data-access/handlers/nconf';
import { activityRowToActivity } from 'data-access/mappers/activity';
import { TopicRow } from 'data-access/handlers/types/dbTypes';
import {
  topicRowToTopic,
  topicRowToTopicName,
} from 'data-access/mappers/topic';
import { ActivitiesDatabaseHandler } from 'data-access/types/ActivitiesDatabaseHandler';

class SQLiteDatabaseHandler implements ActivitiesDatabaseHandler {
  db: Database;

  constructor(
    protected file: string = activitiesDbFilePath(),
    protected mode: number = sqlite3.OPEN_READWRITE,
  ) {}

  public async open(): Promise<void> {
    this.db = await open({
      filename: this.file,
      mode: this.mode,
      driver: sqlite3.Database,
    });
  }

  public async close(): Promise<void> {
    return await this.db.close();
  }

  public async initDb(): Promise<void> {
    await this.db.run(`CREATE TABLE TOPICS(
      -- implicit rowid
      NAME TEXT
    )`);

    await this.db.run('CREATE UNIQUE INDEX topic_names on TOPICS(NAME)');

    await this.db.run(`CREATE TABLE ACTIVITIES(
      -- implicit rowid
      TOPIC_ID INTEGER,
      ACTIVITY TEXT,
      COMMENT  TEXT,
      DAY      TEXT,      -- Day of the activity, ISO 8601 format
      DURATION INTEGER,   -- Number of minutes
      FOREIGN KEY (TOPIC_ID) REFERENCES TOPICS (rowid)
    )`);
  }

  public async insertActivities(activities: Activity[]): Promise<void> {
    const topicIdsIndex: Map<string, number> = await this.getTopicIdsIndex();

    const activitiesParam: object = {};
    const activitiesToInsert: string[] = [];

    activities.forEach((a: Activity, i: number): void => {
      const dateParamName: string = `:date_${i}`;
      const topicParamName: string = `:topic_${i}`;
      const activityParamName: string = `:activity_${i}`;
      const durationParamName: string = `:duration_${i}`;
      const commentParamName: string = `:comment_${i}`;

      activitiesParam[dateParamName] = date.formatDate(a.date);
      activitiesParam[topicParamName] = topicIdsIndex.get(a.topic);
      activitiesParam[activityParamName] = a.activity;
      activitiesParam[durationParamName] = a.duration * 60;
      activitiesParam[commentParamName] = a.comment;
      activitiesToInsert.push(
        `(${dateParamName}, ${topicParamName}, ${activityParamName}, ${durationParamName}, ${commentParamName})`,
      );
    });

    const activitiesToInsertValues: string = activitiesToInsert.join(', ');

    await this.db.run(
      `INSERT INTO ACTIVITIES (DAY, TOPIC_ID, ACTIVITY, DURATION, COMMENT) VALUES ${activitiesToInsertValues}`,
      activitiesParam,
    );
  }

  public async getAllActivities(): Promise<Activity[]> {
    return (
      await this.db.all(`
        SELECT A.rowid AS ID, A.DAY as DAY, T.rowid as TOPIC_ID,
               T.NAME AS TOPIC_NAME, A.ACTIVITY AS ACTIVITY,
               A.DURATION AS DURATION, A.COMMENT AS COMMENT
        FROM ACTIVITIES A JOIN TOPICS T ON A.TOPIC_ID = T.rowid
    `)
    ).map(activityRowToActivity);
  }

  protected async getAllTopicRows(): Promise<TopicRow[]> {
    return await this.db.all('SELECT rowid AS ID, NAME FROM TOPICS');
  }

  public async getAllTopics(): Promise<Topic[]> {
    return (await this.getAllTopicRows()).map(topicRowToTopic);
  }

  public async getTopicIdsIndex(): Promise<Map<string, number>> {
    const topicIdsIndex: Map<string, number> = new Map<string, number>();

    (await this.getAllTopicRows()).forEach((row: TopicRow): void => {
      topicIdsIndex.set(row['NAME'], row['ID']);
    });

    return topicIdsIndex;
  }

  public async getAllTopicsNames(): Promise<string[]> {
    return (await this.getAllTopicRows()).map(topicRowToTopicName);
  }

  public async insertTopics(topicNames: string[]): Promise<void> {
    if (topicNames.length === 0) return;
    const valuesToInsert: string = topicNames
      .map((): string => '(?)')
      .join(', ');

    await this.db.run(
      `INSERT INTO TOPICS (NAME) VALUES ${valuesToInsert}`,
      topicNames,
    );
  }
}

export const getActivityDatabase = async (): ActivitiesDatabaseHandler =>
  new SQLiteDatabaseHandler();

export const initDb = async (file: string): Promise<void> => {
  const dbHandler: ActivitiesDatabaseHandler = new SQLiteDatabaseHandler(
    file,
    sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
  );

  await dbHandler.open();
  await dbHandler.initDb();
  await dbHandler.close();
};
