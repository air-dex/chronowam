/// @author Romain DUCHER<ducher.romain@gmail.com>
///
/// @section LICENSE
///
/// Copyright Romain DUCHER 2023
///
/// This software is a computer program whose purpose is to input working hours
/// and export them in a spreadsheet file.
///
/// This software is governed by the CeCILL-C license under French law and
/// abiding by the rules of distribution of free software. You can use,
/// modify and/ or redistribute the software under the terms of the CeCILL-C
/// license as circulated by CEA, CNRS and INRIA at the following URL
/// "http://www.cecill.info".
///
/// As a counterpart to the access to the source code and rights to copy,
/// modify and redistribute granted by the license, users are provided only
/// with a limited warranty and the software's author, the holder of the
/// economic rights, and the successive licensors have only limited liability.
///
/// In this respect, the user's attention is drawn to the risks associated
/// with loading, using, modifying and/or developing or reproducing the
/// software by the user in light of its specific status of free software,
/// that may mean that it is complicated to manipulate, and that also
/// therefore means that it is reserved for developers and experienced
/// professionals having in-depth computer knowledge. Users are therefore
/// encouraged to load and test the software's suitability as regards their
/// requirements in conditions enabling the security of their systems and/or
/// data to be ensured and, more generally, to use and operate it in the
/// same conditions as regards security.
///
/// The fact that you are presently reading this means that you have had
/// knowledge of the CeCILL-C license and that you accept its terms.
import { fileExists } from 'data-access/handlers/files';
import ConfigValues from 'data-access/handlers/nconf';
import { setNewActivitiesDb } from 'services/config';
import { parseSpreadsheet } from 'data-access/handlers/spreadsheets';
import { Activity } from 'logic/types';
import { getActivityDatabase } from 'data-access/handlers/db';
import { ActivitiesDatabaseHandler } from 'data-access/types/ActivitiesDatabaseHandler';

ConfigValues.load();

export const importActivities = async (
  activityFilePath: string,
  newActivityDb: boolean,
  newActivityDbFilePath: string,
): Promise<Activity[]> => {
  // Creates new activity database file if asked
  if (newActivityDb && !fileExists(newActivityDbFilePath)) {
    await setNewActivitiesDb(newActivityDbFilePath);
  }

  await fetchActivities(activityFilePath);

  const dbHandler: ActivitiesDatabaseHandler = await getActivityDatabase();

  await dbHandler.open();
  const res: Activity[] = await dbHandler.getAllActivities();
  await dbHandler.close();

  return res;
};

export const fetchActivities = async (
  activityFilePath: string,
): Promise<void> => {
  const activities: Activity[] = parseSpreadsheet(activityFilePath);
  await saveActivities(activities);
};

export const saveActivities = async (activities: Activity[]): Promise<void> => {
  if (activities.length === 0) {
    return;
  }

  const dbHandler: ActivitiesDatabaseHandler = await getActivityDatabase();

  await dbHandler.open();

  // Save new topics before
  const newTopics: Set<string> = new Set(
    activities.map((a: Activity): string => a.topic),
  );

  const existingTopics: string[] = await dbHandler.getAllTopicsNames();

  // Removing existing topics for the list to insert
  existingTopics.forEach(newTopics.delete);

  await dbHandler.insertTopics([...newTopics]);

  // Insert activities
  await dbHandler.insertActivities(activities);

  await dbHandler.close();
};
